# SBP Information Package Framework

This document defines the information package framework. It is community developed and depends heavily on open source standards and its published under a MIT license.

## Definitions

Some common terminology is defined within the framework.

| Term                | Definition                                      |
|---------------------|-------------------------------------------------|
| Information package | A container for information that could be processed to one or more desired publications with a extraction process. Information packages is abbreviated to infopack. |
| Extraction process  | The process of extracting publications from a information package |
| Pipeline            | A concept within the extraction process for structuring the operations (steps) that are run over a information package. |
| Generator           | A generator is a small program which runs a step in the pipeline and manipulates the content in some way. |

## Information package

The information package is a core concept within the framework. It is basically a container for information that is subject to processing to one or more publications. In a basic implementation a information package is a folder on a filesystem. The folder can be set under version control with softwares like [GIT](https://git-scm.com/).

### Requirements

The implementations must follow some high level requirements.

#### Infopack definition file

A information package must have definition file. It should be named `infopack.json` and must contain information about:

* Framework version
* Implementation

**Example**

```json
{
    "framework_version": "1.0.0",
    "implementation": "node"
}
```

#### Structure

An information package should be structured in consistent way. The primary reason for this is to provide a clear structure for the generators to operate on. Another reason is to have the possibility to use different implementations of the framework on the same information package.

A information packages does not require all folders to be used or created.

| Folder name    | VC | Description | Typical file formats |
|----------------|----|-------------|----------------------|
| raw_files      | X  | Fragmented information are stored here. Typically structered in a predefined data structure and it needs to be processed prior to usage. Reasons for this fragmentation can be maintainability or for easy filtering/reshaping of data. | `yml`, `json`, `xml` |
| markup_files   | X  | Composed information are stored here. Typically structured in some type of markup language. | `html`, `md` |
| support_files  | X  | Support files (e.g. templates, examples) are stored here | `as needed` |
| output_files   |    | The result of the pipeline should be put here | `pdf`, `docx`, `static website`, `json` |
| work_folder    |    | Temporary data can be put here, this folder is reset for every run |  |

## Extraction process

Information is reshaped/restructured in the extraction process. The framework aims to leave as much freedom as possible to the implementations by not putting detailed requirements. Some basic concepts are defined that helps "harmonize" the implementations.

### Pipeline

### Generators

## Implementations

Implementations of the above standard are listed here.

* [NodeJS - infopack](https://gitlab.com/sbplatform/infopack)
